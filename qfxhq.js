// Quantumult X Script to extract headers and store as a local variable
const headers = $request.headers;
const authorization = headers['authorization'] || headers['Authorization'];
const xdskey = headers['x-ds-key'];

if (authorization && xdskey) {
    const newEntry = `${authorization}&${xdskey}`;
    let qfxhd = $prefs.valueForKey('qfxhd') || '';
    
    // Split the existing entries by '@' to form an array
    let qfxhdArray = qfxhd ? qfxhd.split('@') : [];
    
    // Check if the new entry already exists in the array
    if (!qfxhdArray.includes(newEntry)) {
        // Add the new entry to the array
        qfxhdArray.push(newEntry);
        
        // Join the array back into a single string with '@' as the separator
        qfxhd = qfxhdArray.join('@');
        
        // Save the updated value back to the preferences
        const success = $prefs.setValueForKey(qfxhd, 'qfxhd');
        
        if (success) {
            $notify("获取成功", "已成功提取并保存 headers", qfxhd);
        } else {
            $notify("保存失败", "未能将 headers 保存到偏好设置中", "");
        }
    } else {
        $notify("获取成功", "headers 已经存在，无需重复保存", newEntry);
    }
} else {
    $notify("获取失败", "缺少必要的 headers", "请确保请求中包含 'authorization' 和 'x-ds-key' headers。");
}

$done({});
