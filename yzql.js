const $ = new Env('有赞');
const YouZan = ($.isNode() ? JSON.parse(process.env.YouZan) : $.getjson("YouZan")) || [];
let activityArr = [{"1823831":"小猴工具"},{"3997371":"7.5矿泉水"},{"12063":"SKG会员商城"},{"99":"魅族商城Lite"},{"2162835":"PANDAER 会员商店"},{"3520910":"Achock官方商店"},{"2187565":"蜜蜂惊喜社"},{"2923467":"红之旗舰店"},{"2910869":"FicceCode菲诗蔻官方商城"},{"2386563":"HBN颜究所"},{"1597464":"Xbox 聚乐部"},{"3347128":"松鲜鲜调味品"},{"2299510":"燕京啤酒电商"},{"18415":"得宝Tempo"}];
let notice = '';

!(async () => {
    if (typeof $request !== "undefined") {
        await getCookie();
    } else {
        await main();
    }
})().catch((e) => {$.log(e)}).finally(() => {$.done({});});

async function main() {
    console.log('@BB')
    for (const item of YouZan) {
        let checkinId = item.checkinId;
        let name = checkinId
        if (activityArr.find(item => checkinId in item)) {
            name = activityArr.find(item => checkinId in item)[checkinId];
        }
        console.log(name);
        notice += `${name}\n`;
        for (const data of item.data) {
            let id = data.id, appId = data.appId, kdtId = data.kdtId, token = data.token, extraData = data.extraData;
            console.log(`用户：${id}开始签到`)
            let checkin = await commonGet(`checkinId=${checkinId}&app_id=${appId}&kdt_id=${kdtId}&access_token=${token}`, extraData);
            if (checkin.code === -1) {
                await sendMsg(`${name} 用户：${id}\ntoken已过期，请重新获取`);
                continue
            }
            console.log(`签到结果:${checkin.msg}\n`)
            notice += `用户:${id}  签到结果:${checkin.msg}\n`;
        }
    }
    if (notice) {
        await sendMsg(notice);
    }
}

async function getCookie() {
    let extraData = $request.headers["extra-data"] || $request.headers["Extra-Data"];
    if (!extraData) {
        return
    }
    const urlStr = $request.url.split('?')[1];
    let result = {};
    let paramsArr = urlStr.split('&');
    for (let i = 0, len = paramsArr.length; i < len; i++) {
        let arr = paramsArr[i].split('=');
        result[arr[0]] = arr[1];
    }
    const checkinId = result.checkinId;
    const appId = result.app_id;
    const kdtId = result.kdt_id;
    const token = result.access_token;
    let body = JSON.parse(extraData);
    const id = body.uuid;
    const newData = {"checkinId": checkinId, "data": []};
    const data = {"id": id, "appId": appId, "kdtId": kdtId, "token": token, "extraData": extraData};
    const existingIndex = YouZan.findIndex(e => e.checkinId == newData.checkinId);
    if (existingIndex !== -1) {
        const index = YouZan[existingIndex].data.findIndex(e => e.id == data.id);
        if (index !== -1) {
            if (YouZan[existingIndex].data[index].token == data.token) {
                return
            } else {
                YouZan[existingIndex].data[index] = data;
                console.log(JSON.stringify(data));
                $.msg($.name, `${checkinId}`, `🎉用户${data.id}更新token成功!`);
            }
        } else {
            YouZan[existingIndex].data.push(data);
            console.log(JSON.stringify(data));
            $.msg($.name, `${checkinId}`, `🎉新增用户${data.id}成功!`);
        }
    } else {
        console.log("发现新的签到活动");
        $.msg($.name, `🎉发现新的签到活动!`);
        YouZan.push(newData);
        newData.data.push(data);
        console.log(JSON.stringify(data));
        $.msg($.name, `${checkinId}`, `🎉新增用户${data.id}成功!`);
    }
    $.setjson(YouZan, "YouZan");
}

async function commonGet(url, extraData) {
    return new Promise(resolve => {
        const options = {
            url: `https://h5.youzan.com/wscump/checkin/checkinV2.json?${url}`,
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 10; 16th Build/QKQ1.191222.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/116.0.0.0 Mobile Safari/537.36 XWEB/1160083 MMWEBSDK/20231202 MMWEBID/2933 MicroMessenger/8.0.47.2560(0x28002F50) WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64 MiniProgramEnv/android',
                'Content-Type': 'application/json',
                'Accept-Encoding': 'gzip,compress,br,deflate',
                'Extra-Data': extraData
            }
        };
        $.get(options, async (err, resp, data) => {
            try {
                if (err) {
                    console.log(`${JSON.stringify(err)}`);
                    console.log(`${$.name} API请求失败，请检查网络重试`);
                } else {
                    resolve(JSON.parse(data));
                }
            } catch (e) {
                $.logErr(e, resp);
            } finally {
                resolve();
            }
        })
    })
}

async function sendMsg(message) {
    if ($.isNode()) {
        let notify = require("./sendNotify");
        await notify.sendNotify($.name, message);
    } else {
        $.msg($.name, '', message);
    }
}

// prettier-ignore
function Env(t, e) {
    class s {
        constructor(t) {
            this.env = t
        }
        send(t, e = "GET") {
            t = "string" == typeof t ? { url: t } : t;
            let s = this.get;
            return "POST" === e && (s = this.post), new Promise((e, a) => {
                s.call(this, t, (t, s, r) => {
                    t ? a(t) : e(s)
                })
            })
        }
        get(t) {
            return this.send.call(this.env, t)
        }
        post(t) {
            return this.send.call(this.env, t, "POST")
        }
    }
    return new class {
        constructor(t, e) {
            this.name = t, this.http = new s(this), this.data = null, this.dataFile = "box.dat", this.logs = [], this.isMute = !1, this.isNeedRewrite = !1, this.logSeparator = "\n", this.encoding = "utf-8", this.startTime = (new Date).getTime(), Object.assign(this, e), this.log("", `🔔${this.name}, 开始!`)
        }
        getEnv() {
            return "undefined" != typeof $environment && $environment["surge-version"] ? "Surge" : "undefined" != typeof $environment && $environment["stash-version"] ? "Stash" : "undefined" != typeof module && module.exports ? "Node.js" : "undefined" != typeof $task ? "Quantumult X" : "undefined" != typeof $loon ? "Loon" : "undefined" != typeof $rocket ? "Shadowrocket" : void 0
        }
        isNode() {
            return "Node.js" === this.getEnv()
        }
        isQuanX() {
            return "Quantumult X" === this.getEnv()
        }
        isSurge() {
            return "Surge" === this.getEnv()
        }
        isLoon() {
            return "Loon" === this.getEnv()
        }
        isShadowrocket() {
            return "Shadowrocket" === this.getEnv()
        }
        isStash() {
            return "Stash" === this.getEnv()
        }
        toObj(t, e = null) {
            try {
                return JSON.parse(t)
            } catch {
                return e
            }
        }
        toStr(t, e = null) {
            try {
                return JSON.stringify(t)
            } catch {
                return e
            }
        }
        getjson(t, e) {
            let s = e;
            const a = this.getdata(t);
            if (a) try {
                s = JSON.parse(this.getdata(t))
            } catch { }
            return s
        }
        setjson(t, e) {
            return this.setdata(JSON.stringify(t), e)
        }
        async getScript(t) {
            return new Promise(e => {
                this.get({ url: t }, (t, s, a) => e(a))
            })
        }
        runScript(t, e) {
            return new Promise(s => {
                let a = this.getdata("@chavy_boxjs_userCfgs.httpapi");
                a = a ? a.replace(/\n/g, "").trim() : a;
                let i = this.getdata("@chavy_boxjs_userCfgs.httpapi_timeout");
                i = i ? 1 * i : 20, i = e && e.timeout ? e.timeout : i;
                const [r, o] = a.split("@"), n = {
                    url: `http://${o}/v1/scripting/evaluate`,
                    body: { script_text: t, mock_type: "cron", timeout: i },
                    headers: { "X-Key": r, Accept: "*/*" }
                };
                this.post(n, (t, e, a) => s(a))
            }).catch(t => this.logErr(t))
        }
        loaddata() {
            if (!this.isNode()) return {};
            {
                this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path");
                const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), a = !s && this.fs.existsSync(e);
                if (!s && !a) return {};
                {
                    const a = s ? t : e;
                    try {
                        return JSON.parse(this.fs.readFileSync(a))
                    } catch (t) {
                        return {}
                    }
                }
            }
        }
        writedata() {
            if (this.isNode()) {
                this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path");
                const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), a = !s && this.fs.existsSync(e), i = JSON.stringify(this.data);
                s ? this.fs.writeFileSync(t, i) : a ? this.fs.writeFileSync(e, i) : this.fs.writeFileSync(t, i)
            }
        }
        lodash_get(t, e, s) {
            const a = e.replace(/\[(\d+)\]/g, ".$1").split(".");
            let i = t;
            for (const t of a) if (i = Object(i)[t], void 0 === i) return s;
            return i
        }
        lodash_set(t, e, s) {
            return Object(t) !== t ? t : (Array.isArray(e) || (e = e.toString().match(/[^.[\]]+/g) || []), e.slice(0, -1).reduce((t, s, a) => Object(t[s]) === t[s] ? t[s] : t[s] = Math.abs(e[a + 1]) >> 0 == +e[a + 1] ? [] : {}, t)[e[e.length - 1]] = s, t)
        }
        getdata(t) {
            let e = this.getval(t);
            if (/^@/.test(t)) {
                const [, s, a] = /^@(.*?)\.(.*?)$/.exec(t), i = s ? this.getval(s) : "";
                if (i) try {
                    const t = JSON.parse(i);
                    e = t ? this.lodash_get(t, a, "") : e
                } catch (t) {
                    e = ""
                }
            }
            return e
        }
        setdata(t, e) {
            let s = !1;
            if (/^@/.test(e)) {
                const [, a, i] = /^@(.*?)\.(.*?)$/.exec(e), r = this.getval(a), o = a ? "null" === r ? null : r || "{}" : "{}";
                try {
                    const e = JSON.parse(o);
                    this.lodash_set(e, i, t), s = this.setval(JSON.stringify(e), a)
                } catch (e) {
                    const r = {};
                    this.lodash_set(r, i, t), s = this.setval(JSON.stringify(r), a)
                }
            } else s = this.setval(t, e);
            return s
        }
        getval(t) {
            return this.isSurge() || this.isLoon() || this.isShadowrocket() ? $persistentStore.read(t) : this.isQuanX() ? $prefs.valueForKey(t) : this.isNode() ? (this.data = this.loaddata(), this.data[t]) : this.data && this.data[t] || null
        }
        setval(t, e) {
            return this.isSurge() || this.isLoon() || this.isShadowrocket() ? $persistentStore.write(t, e) : this.isQuanX() ? $prefs.setValueForKey(t, e) : this.isNode() ? (this.data = this.loaddata(), this.data[e] = t, this.writedata(), !0) : this.data && this.data[e] || null
        }
        initGotEnv(t) {
            this.got = this.got ? this.got : require("got"), this.cktough = this.cktough ? this.cktough : require("tough-cookie"), this.ckjar = this.ckjar ? this.ckjar : new this.cktough.CookieJar, t && (t.headers = t.headers ? t.headers : {}, void 0 === t.headers.Cookie && void 0 === t.cookieJar && (t.cookieJar = this.ckjar))
        }
        get(t, e = (() => { })) {
            t.headers && (delete t.headers["Content-Type"], delete t.headers["Content-Length"]), this.isSurge() || this.isLoon() || this.isShadowrocket() ? (this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.get(t, (t, s, a) => {
                !t && s && (s.body = a, s.statusCode = s.status), e(t, s, a)
            })) : this.isQuanX() ? (this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => {
                const { statusCode: s, statusCode: a, headers: i, body: r } = t;
                e(null, { status: s, statusCode: a, headers: i, body: r }, r)
            }, t => e(t && t.response && t.response.statusCode, t && t.response && t.response.headers, t && t.response && t.response.body))) : this.isNode() && (this.initGotEnv(t), this.got(t).on("redirect", (t, e) => {
                try {
                    if (t.headers["set-cookie"]) {
                        const s = t.headers["set-cookie"].map(this.cktough.Cookie.parse).toString();
                        s && this.ckjar.setCookieSync(s, null), e.cookieJar = this.ckjar
                    }
                } catch (t) {
                    this.logErr(t)
                }
            }).then(t => {
                const { statusCode: s, statusCode: a, headers: i, body: r } = t;
                e(null, { status: s, statusCode: a, headers: i, body: r }, r)
            }, t => {
                const { message: s, response: a } = t;
                e(s, a, a && a.body)
            }))
        }
        post(t, e = (() => { })) {
            if (t.body && t.headers && !t.headers["Content-Type"] && (t.headers["Content-Type"] = "application/x-www-form-urlencoded"), t.headers && delete t.headers["Content-Length"], this.isSurge() || this.isLoon() || this.isShadowrocket()) this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.post(t, (t, s, a) => {
                !t && s && (s.body = a, s.statusCode = s.status), e(t, s, a)
            }); else if (this.isQuanX()) t.method = "POST", this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => {
                const { statusCode: s, statusCode: a, headers: i, body: r } = t;
                e(null, { status: s, statusCode: a, headers: i, body: r }, r)
            }, t => e(t && t.response && t.response.statusCode, t && t.response && t.response.headers, t && t.response && t.response.body)); else if (this.isNode()) {
                this.initGotEnv(t);
                const { url: s, ...a } = t;
                this.got.post(s, a).then(t => {
                    const { statusCode: s, statusCode: a, headers: i, body: r } = t;
                    e(null, { status: s, statusCode: a, headers: i, body: r }, r)
                }, t => {
                    const { message: s, response: a } = t;
                    e(s, a, a && a.body)
                })
            }
        }
        time(t, e = null) {
            const s = e ? new Date(e) : new Date;
            let a = {
                "M+": s.getMonth() + 1,
                "d+": s.getDate(),
                "H+": s.getHours(),
                "m+": s.getMinutes(),
                "s+": s.getSeconds(),
                "q+": Math.floor((s.getMonth() + 3) / 3),
                S: s.getMilliseconds()
            };
            /(y+)/.test(t) && (t = t.replace(RegExp.$1, (s.getFullYear() + "").substr(4 - RegExp.$1.length)));
            for (let e in a) new RegExp("(" + e + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? a[e] : ("00" + a[e]).substr(("" + a[e]).length)));
            return t
        }
        msg(e = t, s = "", a = "", i) {
            const r = t => {
                if (!t) return t;
                if ("string" == typeof t) return this.isLoon() ? t : this.isQuanX() ? { "open-url": t } : this.isSurge() ? { url: t } : void 0;
                if ("object" == typeof t) {
                    if (this.isLoon()) {
                        let e = t.openUrl || t.url || t["open-url"], s = t.mediaUrl || t["media-url"];
                        return { openUrl: e, mediaUrl: s }
                    }
                    if (this.isQuanX()) {
                        let e = t["open-url"] || t.url || t.openUrl, s = t["media-url"] || t.mediaUrl;
                        return { "open-url": e, "media-url": s }
                    }
                    if (this.isSurge()) {
                        let e = t.url || t.openUrl || t["open-url"];
                        return { url: e }
                    }
                }
            };
            if (this.isMute || (this.isSurge() || this.isLoon() ? $notification.post(e, s, a, r(i)) : this.isQuanX() && $notify(e, s, a, r(i))), !this.isMuteLog) {
                let t = ["", "==============📣系统通知📣=============="];
                t.push(e), s && t.push(s), a && t.push(a), console.log(t.join("\n")), this.logs = this.logs.concat(t)
            }
        }
        log(...t) {
            t.length > 0 && (this.logs = [...this.logs, ...t]), console.log(t.join(this.logSeparator))
        }
        logErr(t, e) {
            const s = !this.isSurge() && !this.isQuanX() && !this.isLoon();
            s ? this.log("", `❗️${this.name}, 错误!`, t.stack) : this.log("", `❗️${this.name}, 错误!`, t)
        }
        wait(t) {
            return new Promise(e => setTimeout(e, t))
        }
        done(t = {}) {
            const e = (new Date).getTime(), s = (e - this.startTime) / 1e3;
            this.log("", `🔔${this.name}, 结束! 🕛 ${s} 秒`), this.log(), (this.isSurge() || this.isQuanX() || this.isLoon()) && $done(t)
        }
    }(t, e)
}
